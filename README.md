My aim - design 32bit computer in Hutton32 CA.

[`1. elementary basis:`](https://github.com/FredericaBernkastel/Cellular-automaton/blob/master/JvN/circuits-sandbox.mc)

![](https://raw.githubusercontent.com/FredericaBernkastel/Cellular-automaton/master/JvN/circuits-sandbox.png)
<br><br><br>
[`2. 8-bit decimal counter:`](https://github.com/FredericaBernkastel/Cellular-automaton/blob/master/JvN/advanced%20counter.mc)

![](https://raw.githubusercontent.com/FredericaBernkastel/Cellular-automaton/master/JvN/advanced%20counter~marked.png)
<br><br><br>
[`3. 16x16 display:`](https://github.com/FredericaBernkastel/Cellular-automaton/blob/master/JvN/big%20sandbox/16x16%20display.mc)

![](https://raw.githubusercontent.com/FredericaBernkastel/Cellular-automaton/master/JvN/big%20sandbox/16x16%20display.png)